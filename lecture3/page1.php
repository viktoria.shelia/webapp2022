<h1>Lecture 3</h1>


<?php
    class person{
        public $name;
        public $last_name;
        private $age = 19;
        protected $id = "12133112";

        protected function print_php_info(){
            var_dump(phpinfo());
        }


        private function print_class_name(){
            echo "<h3>Person</h3>";
        }


        public function print_name(){
            echo $this->name;
        }

        function print_age(){
            $this->print_class_name();
            echo $this->age;
        }

        function get_age(){
            return $this->age;
        }


    }


    $person1 = new person();
    $person1->name = "Victoria";

    $person1->print_age();
    $age2 = $person1->get_age() + 10;
    echo "<br>age - {$age2}";
    //$per

?>
